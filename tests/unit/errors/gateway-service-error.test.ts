import { expect } from 'chai';
import 'mocha';
import { GatewayServiceError } from '../../../lib/errors/gateway-service-error';

describe('check GatewayServiceError', () => {

    it('check constructor', () => {

        const testError = new GatewayServiceError.Builder('Test message')
            .withError('Test error')
            .withCause('Test cause')
            .withStatusCode(500)
            .withPath('Test url')
            .withStackTrace(['stack trace', 'test'])
            .build();

        expect(testError.getError()).to.be.equal('Test error');
        expect(testError.getMessage()).to.be.equal('Test message');
        expect(testError.getCause()).to.be.equal('Test cause');
        expect(testError.getStatusCode()).to.be.equal(500);
        expect(testError.getPath()).to.be.equal('Test url');

        const stackTrace = testError.getStackTrace();
        expect(stackTrace.length).to.be.equal(2);
        expect(stackTrace[0]).to.be.equal('stack trace');
        expect(stackTrace[1]).to.be.equal('test');
    });
});
