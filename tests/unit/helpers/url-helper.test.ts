import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { UrlHelper } from '../../../lib/helpers/url-helper';
import { MethodExecutionDescriptor } from '../../../lib/types/method-execution-descriptor';
import { MetadataHelper } from '../../../lib/helpers/metadata-helper';

describe('check UrlHelper', () => {
    const testStubs = [];
    const testExecutionDescriptor: MethodExecutionDescriptor = {
        target: null,
        propertyKey: 'test property',
        arguments: { '0': 1, '1': 2, '2': { testKey: 'testValue' }, length: 1, callee: null, *[Symbol.iterator]() { } }
    };

    before(function() {

        testStubs.push(
            sinon.stub(MetadataHelper, 'getPathParamsMetadatata').callsFake(() => ([
                { parameterIndex: 0, identifier: 'id' },
                { parameterIndex: 1, identifier: 'group' }
            ])),
            sinon.stub(MetadataHelper, 'getQueryParamIndexMetadata').callsFake(() => 2)
        );
    });

	after(function() {

        testStubs.forEach((stub) => { stub.restore(); });
    });

    it('check replacePathParameters method', () => {

        const updatedUrl = UrlHelper.replacePathParameters('/path/${id}/${group}', testExecutionDescriptor);
        expect(updatedUrl).to.be.equal('/path/1/2');
    });

    it('check addQueryParameters method', () => {

        const updatedUrl = UrlHelper.addQueryParameters('/path', testExecutionDescriptor);
        expect(updatedUrl).to.be.equal('/path?testKey=testValue');
    });

    it('check extractUrlComponents method', () => {

        expect(UrlHelper.extractUrlComponents('http://localhost:8080/players')).to.deep.equal({
            hostname: 'localhost',
            port: 8080,
            path: '/players',
            method: '',
            headers: {}
        });

        expect(UrlHelper.extractUrlComponents('localhost/')).to.deep.equal({
            hostname: 'localhost',
            port: 80,
            path: '/',
            method: '',
            headers: {}
        });

        expect(UrlHelper.extractUrlComponents('localhost/players')).to.deep.equal({
            hostname: 'localhost',
            port: 80,
            path: '/players',
            method: '',
            headers: {}
        });

        expect(UrlHelper.extractUrlComponents('http://localhost/players')).to.deep.equal({
            hostname: 'localhost',
            port: 80,
            path: '/players',
            method: '',
            headers: {}
        });

        expect(UrlHelper.extractUrlComponents('localhost:8080/players')).to.deep.equal({
            hostname: 'localhost',
            port: 8080,
            path: '/players',
            method: '',
            headers: {}
        });

        expect(UrlHelper.extractUrlComponents('http://localhost:8080/players/1')).to.deep.equal({
            hostname: 'localhost',
            port: 8080,
            path: '/players/1',
            method: '',
            headers: {}
        });
    });
});
