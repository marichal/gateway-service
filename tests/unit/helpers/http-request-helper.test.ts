import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import * as Http from 'http';
import { HttpRequestHelper } from '../../../lib/helpers/http-request-helper';
import { HttpRequestParameters } from '../../../lib/types/http-request-parameters';
import { UrlHelper } from '../../../lib/helpers/url-helper';
import { HttpResponseHelper } from '../../../lib/helpers/http-response-helper';
import { MetadataHelper } from '../../../lib/helpers/metadata-helper';
import { MethodExecutionDescriptor } from '../../../lib/types/method-execution-descriptor';

describe('check HttpRequestHelper', () => {
    const testStubs = [];

    before(function() {

        testStubs.push(
            sinon.stub(UrlHelper, 'extractUrlComponents').callsFake(() => ({})),
            sinon.stub(Http, 'request').callsFake((requestOptions, callback) => { callback(null, { response: 'test response' }); }),
            sinon.stub(HttpResponseHelper, 'processHttpResponse').callsFake(
                (httpResponse, httpRequestParameters, callback) => { callback(null, { response: 'test response' });
            }),
            sinon.stub(MetadataHelper, 'getBodyParamIndexMetadata').callsFake(() => 0)
        );
    });

	after(function() {

        testStubs.forEach((stub) => { stub.restore(); });
    });

    it('check doRequest method', () => {

        const httpRequestParameters: HttpRequestParameters = {
            method: 'GET',
            httpMethodParameters: { url: 'test url' },
            expectedStatusCodes: [200],
            expectedContentTypes: ['application/json']
        };

        return HttpRequestHelper.doRequest(httpRequestParameters)
		.then((response) => {
			expect(response).to.deep.equal({ response: 'test response' });
		});
    });

    it('check getBodyObject method', () => {

        const testExecutionDescriptor: MethodExecutionDescriptor = {
            target: null,
            propertyKey: 'test property',
            arguments: { '0': { content: 'test body' }, length: 1, callee: null, *[Symbol.iterator]() { } }
        };

        const returnedBody = HttpRequestHelper.getBodyObject(testExecutionDescriptor);
        expect(returnedBody).to.deep.equal({ content: 'test body' });
    });
});
