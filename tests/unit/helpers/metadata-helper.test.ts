import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { MetadataHelper } from '../../../lib/helpers/metadata-helper';
import { MethodExecutionDescriptor } from '../../../lib/types/method-execution-descriptor';

describe('check MetadataHelper', () => {
    const testExecutionDescriptor: MethodExecutionDescriptor = {
        target: this,
        propertyKey: 'test property',
        arguments: { '0': { content: 'test body' }, length: 1, callee: null, *[Symbol.iterator]() { } }
    };

    it('check path parameters metadata', () => {

        MetadataHelper.setPathParamsMetadata(testExecutionDescriptor, [{ identifier: 'testIdentifier', parameterIndex: 0}]);
        const pathMetadata = MetadataHelper.getPathParamsMetadatata(testExecutionDescriptor);
        expect(pathMetadata.length).to.be.equal(1);
        expect(pathMetadata[0].identifier).to.be.equal('testIdentifier');
        expect(pathMetadata[0].parameterIndex).to.be.equal(0);
    });

    it('check query parameters index metadata', () => {

        MetadataHelper.setQueryParamIndexMetadata(testExecutionDescriptor, 1);
        expect(MetadataHelper.getQueryParamIndexMetadata(testExecutionDescriptor)).to.be.equal(1);
    });

    it('check mapping metadata', () => {

        const testFunction = sinon.spy();
        MetadataHelper.setMappingMetadata(testExecutionDescriptor, testFunction);
        expect(MetadataHelper.getMappingMetadata(testExecutionDescriptor)).to.be.equal(testFunction);
    });

    it('check body parameter index metadata', () => {

        MetadataHelper.setBodyParamIndexMetadata(testExecutionDescriptor, 1);
        expect(MetadataHelper.getBodyParamIndexMetadata(testExecutionDescriptor)).to.be.equal(1);
    });

    it('check message metadata', () => {

        MetadataHelper.setMessageParamIndexMetadata(testExecutionDescriptor, 1);
        expect(MetadataHelper.getMessageParamIndexMetadata(testExecutionDescriptor)).to.be.equal(1);
    });

    it('check subscription callback metadata', () => {

        MetadataHelper.setSubscriptionCallbackParamIndexMetadata(testExecutionDescriptor, 1);
        expect(MetadataHelper.getSubscriptionCallbackParamIndexMetadata(testExecutionDescriptor)).to.be.equal(1);
    });

    it('check disconnection callback metadata', () => {

        MetadataHelper.setDisconnectionCallbackParamIndexMetadata(testExecutionDescriptor, 1);
        expect(MetadataHelper.getDisconnectionCallbackParamIndexMetadata(testExecutionDescriptor)).to.be.equal(1);
    });
});
