import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { Client, client } from '@stomp/stompjs';
import { WebSocketHelper } from '../../../lib/helpers/websocket-helper';
import { MetadataHelper } from '../../../lib/helpers/metadata-helper';
import { MethodExecutionDescriptor } from '../../../lib/types/method-execution-descriptor';
import { WebSocketMethodParameters } from '../../../lib/types/websocket-method-parameters';

describe('check WebSocketHelper', () => {
    const testStubs = [];
    let mockWebSocketClient: Client;
    let helperMethodParams = {};

    before(function() {

        testStubs.push(
            sinon.stub(MetadataHelper, 'getMessageParamIndexMetadata').callsFake(() => 0),
            sinon.stub(MetadataHelper, 'getSubscriptionCallbackParamIndexMetadata').callsFake(() => 0),
            sinon.stub(MetadataHelper, 'getDisconnectionCallbackParamIndexMetadata').callsFake(() => 0),
        );

        mockWebSocketClient = client('test url');
        mockWebSocketClient.connected = true;

        testStubs.push(
            sinon.stub(mockWebSocketClient, 'send')
                .callsFake((destination, headers, message) => { helperMethodParams = { destination, headers, message }; }),
            sinon.stub(mockWebSocketClient, 'subscribe').callsFake(() => { }),
            sinon.stub(mockWebSocketClient, 'disconnect').callsFake(() => { })
        );
    });

	after(function() {

        testStubs.forEach((stub) => { stub.restore(); });
    });

    it('check getWebSocketMessage method', () => {

        const testExecutionDescriptor: MethodExecutionDescriptor = {
            target: null,
            propertyKey: 'test property',
            arguments: { '0': '{ message: "test message" }', length: 1, callee: null, *[Symbol.iterator]() { } }
        };

        const returnedMessage = WebSocketHelper.getWebSocketMessage(testExecutionDescriptor);
        expect(returnedMessage).to.be.equal('{ message: "test message" }');
    });

    it('check getSubscriptionCallback method', () => {

        const fakeFunction = () => {};
        const testExecutionDescriptor: MethodExecutionDescriptor = {
            target: null,
            propertyKey: 'test property',
            arguments: { '0': fakeFunction, length: 1, callee: null, *[Symbol.iterator]() { } }
        };

        const returnedMessage = WebSocketHelper.getSubscriptionCallback(testExecutionDescriptor);
        expect(returnedMessage).to.be.equal(fakeFunction);
    });

    it('check getDisconnectionCallback method', () => {

        const fakeFunction = () => {};
        const testExecutionDescriptor: MethodExecutionDescriptor = {
            target: null,
            propertyKey: 'test property',
            arguments: { '0': fakeFunction, length: 1, callee: null, *[Symbol.iterator]() { } }
        };

        const returnedMessage = WebSocketHelper.getDisconnectionCallback(testExecutionDescriptor);
        expect(returnedMessage).to.be.equal(fakeFunction);
    });

    it('check sendMessage method', () => {

        const webSocketMethodParameters: WebSocketMethodParameters = {
            webSocketClient: mockWebSocketClient,
            isDebugActive: true,
            connectionHeaders: {},
            outgoingMessage: 'test message'
        };

        return WebSocketHelper.sendMessage(webSocketMethodParameters, 'test destination')
        .then(() => {
            expect(helperMethodParams).to.deep.equal({
                destination: 'test destination',
                headers: {},
                message: 'test message'
            });
        });
    });

    it('check subscribeToDestination method', () => {

        const fakeCallback = () => {};
        const webSocketMethodParameters: WebSocketMethodParameters = {
            webSocketClient: mockWebSocketClient,
            isDebugActive: true,
            connectionHeaders: {},
            subscriptionCallback: fakeCallback
        };

        return WebSocketHelper.subscribeToDestination(webSocketMethodParameters, 'test destination')
        .then(() => {
            expect(testStubs[4].called).to.be.equal(true);
        });
    });

    it('check disconnect method', () => {

        const fakeCallback = () => {};
        const webSocketMethodParameters: WebSocketMethodParameters = {
            webSocketClient: mockWebSocketClient,
            isDebugActive: true,
            connectionHeaders: {},
            disconnectionCallback: fakeCallback
        };

        return WebSocketHelper.disconnect(webSocketMethodParameters)
        .then(() => {
            expect(testStubs[5].called).to.be.equal(true);
        });
    });
});
