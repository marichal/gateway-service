import { expect } from 'chai';
import 'mocha';
import { HttpRequestParameters } from '../../../lib/types/http-request-parameters';
import { HttpResponseHelper } from '../../../lib/helpers/http-response-helper';

describe('check HttpResponseHelper', () => {

    it('check processHttpResponse method', () => {

        const httpRequestParameters: HttpRequestParameters = {
            method: 'GET',
            httpMethodParameters: { url: 'test url' },
            expectedStatusCodes: [200],
            expectedContentTypes: ['application/json']
        };

        const testResponse = {
            headers: {
                'content-type': 'application/json',
            },
            statusCode: 200,
            setEncoding: () => {},
            on: (event, callback) => {
                switch (event) {
                    case 'data': callback(`{ "content": "test content" }`); break;
                    case 'end': callback(); break;
                }
            }
        };

        return HttpResponseHelper.processHttpResponse(testResponse, httpRequestParameters, (error, response) => {

            expect(error).to.be.equal(null);
            expect(response).to.deep.equal({ content: 'test content' });
        });
    });
});
