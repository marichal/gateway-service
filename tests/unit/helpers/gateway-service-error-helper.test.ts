import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { GatewayServiceErrorHelper } from '../../../lib/helpers/gateway-service-error-helper';
import { GatewayServiceError } from '../../../lib/errors/gateway-service-error';

describe('check GatewayServiceErrorHelper', () => {
    const rejectFunctionSpy = sinon.spy();

    it('check prepareHttpErrorAndReject after generic error', (done) => {

        GatewayServiceErrorHelper
            .prepareHttpErrorAndReject('Generic test error', 'Test url', (error: GatewayServiceError) => {

                expect(error.getMessage()).to.be.equal('Generic test error');
                expect(error.getPath()).to.be.equal('Test url');
                expect(error.getStatusCode()).to.be.equal(500);
                expect(error.getStackTrace().length).to.be.equal(0);

                done();
            });
    });

    it('check prepareHttpErrorAndReject after GatewayServiceError error', (done) => {

        const gatewayServiceError = new GatewayServiceError.Builder('Test message')
            .withError('Test error')
            .withCause('Test cause')
            .withStatusCode(500)
            .withPath('Test url')
            .withStackTrace(['stack trace', 'test'])
            .build();

        GatewayServiceErrorHelper
            .prepareHttpErrorAndReject(gatewayServiceError, null, (error: GatewayServiceError) => {

                expect(gatewayServiceError.getError()).to.be.equal('Test error');
                expect(gatewayServiceError.getMessage()).to.be.equal('Test message');
                expect(gatewayServiceError.getCause()).to.be.equal('Test cause');
                expect(gatewayServiceError.getStatusCode()).to.be.equal(500);
                expect(gatewayServiceError.getPath()).to.be.equal('Test url');

                const stackTrace = gatewayServiceError.getStackTrace();
                expect(stackTrace.length).to.be.equal(2);
                expect(stackTrace[0]).to.be.equal('stack trace');
                expect(stackTrace[1]).to.be.equal('test');

                done();
            });
    });
});
