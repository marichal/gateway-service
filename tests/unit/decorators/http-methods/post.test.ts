import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { Post } from '../../../../lib/decorators/http-methods/post';
import { HttpMethodExecutor } from '../../../../lib/decorators/executors/http-method-executor';

describe('check Post decorator', () => {
    const testStubs = [];
    let httpExecutorSpy;

	before(function() {

        httpExecutorSpy = sinon.spy(HttpMethodExecutor.prototype, 'execute');
	});

	after(function() {

        httpExecutorSpy.restore();
    });

    it('check HTTP executor invocation', () => {

        Post('testUrl');
        expect(httpExecutorSpy.called).to.equal(true);
    });
});
