import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { Patch } from '../../../../lib/decorators/http-methods/patch';
import { HttpMethodExecutor } from '../../../../lib/decorators/executors/http-method-executor';

describe('check Patch decorator', () => {
    const testStubs = [];
    let httpExecutorSpy;

	before(function() {

        httpExecutorSpy = sinon.spy(HttpMethodExecutor.prototype, 'execute');
	});

	after(function() {

        httpExecutorSpy.restore();
    });

    it('check HTTP executor invocation', () => {

        Patch('testUrl');
        expect(httpExecutorSpy.called).to.equal(true);
    });
});
