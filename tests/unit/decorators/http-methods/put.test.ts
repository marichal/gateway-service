import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { Put } from '../../../../lib/decorators/http-methods/put';
import { HttpMethodExecutor } from '../../../../lib/decorators/executors/http-method-executor';

describe('check Put decorator', () => {
    const testStubs = [];
    let httpExecutorSpy;

	before(function() {

        httpExecutorSpy = sinon.spy(HttpMethodExecutor.prototype, 'execute');
	});

	after(function() {

        httpExecutorSpy.restore();
    });

    it('check HTTP executor invocation', () => {

        Put('testUrl');
        expect(httpExecutorSpy.called).to.equal(true);
    });
});
