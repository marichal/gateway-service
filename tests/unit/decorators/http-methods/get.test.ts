import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { Get } from '../../../../lib/decorators/http-methods/get';
import { HttpMethodExecutor } from '../../../../lib/decorators/executors/http-method-executor';

describe('check Get decorator', () => {
    const testStubs = [];
    let httpExecutorSpy;

	before(function() {

        httpExecutorSpy = sinon.spy(HttpMethodExecutor.prototype, 'execute');
	});

	after(function() {

        httpExecutorSpy.restore();
    });

    it('check HTTP executor invocation', () => {

        Get('testUrl');
        expect(httpExecutorSpy.called).to.equal(true);
    });
});
