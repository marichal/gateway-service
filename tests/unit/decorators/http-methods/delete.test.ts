import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { Delete } from '../../../../lib/decorators/http-methods/delete';
import { HttpMethodExecutor } from '../../../../lib/decorators/executors/http-method-executor';

describe('check Delete decorator', () => {
    const testStubs = [];
    let httpExecutorSpy;

	before(function() {

        httpExecutorSpy = sinon.spy(HttpMethodExecutor.prototype, 'execute');
	});

	after(function() {

        httpExecutorSpy.restore();
    });

    it('check HTTP executor invocation', () => {

        Delete('testUrl');
        expect(httpExecutorSpy.called).to.equal(true);
    });
});
