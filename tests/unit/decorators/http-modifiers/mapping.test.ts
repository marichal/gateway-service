import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { MetadataHelper } from '../../../../lib/helpers/metadata-helper';
import { Mapping } from '../../../../lib/decorators/http-modifiers/mapping';

describe('check Mapping decorator', () => {
    const testStubs = [];
    let testDescriptor;
    const mockMapper = sinon.spy();

	before(function() {

        testStubs.push(
            sinon.stub(MetadataHelper, 'setMappingMetadata').callsFake((descriptor, mapper) => {
                testDescriptor = descriptor;
                mapper();
            })
        );
	});

	after(function() {

        testStubs.forEach((stub) => { stub.restore(); });
    });

    it('check metadata setting', () => {

        Mapping(mockMapper)('mockTarget', 'mockProperty', null);

        expect(testDescriptor.target).to.be.equal('mockTarget');
        expect(testDescriptor.propertyKey).to.be.equal('mockProperty');
        expect(mockMapper.called).to.be.equal(true);
    });
});
