import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { MetadataHelper } from '../../../../lib/helpers/metadata-helper';
import { Body } from '../../../../lib/decorators/http-modifiers/body';

describe('check Body decorator', () => {
    const testStubs = [];
    let testDescriptor;
    let testIndex;

	before(function() {

        testStubs.push(
            sinon.stub(MetadataHelper, 'getBodyParamIndexMetadata').callsFake(() => null),
            sinon.stub(MetadataHelper, 'setBodyParamIndexMetadata').callsFake((descriptor, index) => {
                testDescriptor = descriptor;
                testIndex = index;
            })
        );
	});

	after(function() {

        testStubs.forEach((stub) => { stub.restore(); });
    });

    it('check metadata setting', () => {

        Body('mockTarget', 'mockProperty', 1);

        expect(testDescriptor.target).to.be.equal('mockTarget');
        expect(testDescriptor.propertyKey).to.be.equal('mockProperty');
        expect(testIndex).to.be.equal(1);
    });
});
