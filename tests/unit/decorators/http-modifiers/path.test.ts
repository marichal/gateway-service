import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { MetadataHelper } from '../../../../lib/helpers/metadata-helper';
import { Path } from '../../../../lib/decorators/http-modifiers/path';

describe('check Path decorator', () => {
    const testStubs = [];
    let testDescriptor;
    let testPathParameter;

	before(function() {

        testStubs.push(
            sinon.stub(MetadataHelper, 'getPathParamsMetadatata').callsFake(() => null),
            sinon.stub(MetadataHelper, 'setPathParamsMetadata').callsFake((descriptor, pathParameter) => {
                testDescriptor = descriptor;
                testPathParameter = pathParameter;
            })
        );
	});

	after(function() {

        testStubs.forEach((stub) => { stub.restore(); });
    });

    it('check metadata setting', () => {

        Path('testIdentifier')('mockTarget', 'mockProperty', 1);

        expect(testDescriptor.target).to.be.equal('mockTarget');
        expect(testDescriptor.propertyKey).to.be.equal('mockProperty');
        expect(testPathParameter.length).to.be.equal(1);
        expect(testPathParameter[0].parameterIndex).to.be.equal(1);
        expect(testPathParameter[0].identifier).to.be.equal('testIdentifier');
    });
});
