import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { MetadataHelper } from '../../../../lib/helpers/metadata-helper';
import { QueryParameters } from '../../../../lib/decorators/http-modifiers/query-parameters';

describe('check QueryParameters decorator', () => {
    const testStubs = [];
    let testDescriptor;
    let testIndex;

	before(function() {

        testStubs.push(
            sinon.stub(MetadataHelper, 'getQueryParamIndexMetadata').callsFake(() => null),
            sinon.stub(MetadataHelper, 'setQueryParamIndexMetadata').callsFake((descriptor, index) => {
                testDescriptor = descriptor;
                testIndex = index;
            })
        );
	});

	after(function() {

        testStubs.forEach((stub) => { stub.restore(); });
    });

    it('check metadata setting', () => {

        QueryParameters('mockTarget', 'mockProperty', 1);

        expect(testDescriptor.target).to.be.equal('mockTarget');
        expect(testDescriptor.propertyKey).to.be.equal('mockProperty');
        expect(testIndex).to.be.equal(1);
    });
});
