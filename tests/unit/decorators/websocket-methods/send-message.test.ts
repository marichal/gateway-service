import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { SendMessage } from '../../../../lib/decorators/websocket-methods/send-message';
import { WebSocketMethodExecutor } from '../../../../lib/decorators/executors/websocket-method-executor';

describe('check SendMessage decorator', () => {
    const testStubs = [];
    let webSocketExecutorSpy;

	before(function() {

        webSocketExecutorSpy = sinon.spy(WebSocketMethodExecutor.prototype, 'execute');
	});

	after(function() {

        webSocketExecutorSpy.restore();
    });

    it('check WebSocket executor invocation', () => {

        SendMessage('test destination');
        expect(webSocketExecutorSpy.called).to.equal(true);
    });
});
