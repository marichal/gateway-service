import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { Subscribe } from '../../../../lib/decorators/websocket-methods/subscribe';
import { WebSocketMethodExecutor } from '../../../../lib/decorators/executors/websocket-method-executor';

describe('check Subscribe decorator', () => {
    const testStubs = [];
    let webSocketExecutorSpy;

	before(function() {

        webSocketExecutorSpy = sinon.spy(WebSocketMethodExecutor.prototype, 'execute');
	});

	after(function() {

        webSocketExecutorSpy.restore();
    });

    it('check WebSocket executor invocation', () => {

        Subscribe('test destination');
        expect(webSocketExecutorSpy.called).to.equal(true);
    });
});
