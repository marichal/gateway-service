import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { DisconnectWebSocket } from '../../../../lib/decorators/websocket-methods/disconnect-websocket';
import { GatewayServiceError } from '../../../../lib/errors/gateway-service-error';
import { WebSocketHelper } from '../../../../lib/helpers/websocket-helper';

describe('check DisconnectWebSocket decorator', () => {
    const testStubs = [];

	before(function() {

        testStubs.push(
            sinon.stub(WebSocketHelper, 'getDisconnectionCallback').callsFake(() => 'mockDisconnectionCallback'),
            sinon.stub(WebSocketHelper, 'disconnect').callsFake(() => Promise.resolve())
        );
	});

	after(function() {

        testStubs.forEach((stub) => { stub.restore(); });
    });

    it('check execute method on a non GatewayServiceWebSocket object', (done) => {

        const testPropertyDescriptor  = {};
        DisconnectWebSocket('testTarget', 'testPropertyKey', testPropertyDescriptor);

        testPropertyDescriptor['value']()
        .catch((error) => {
            expect(error instanceof GatewayServiceError);
            done();
        });
    });

    it('check execute method on a GatewayServiceWebSocket object', (done) => {

        const testPropertyDescriptor  = {};
        DisconnectWebSocket('testTarget', 'testPropertyKey', testPropertyDescriptor);

        testPropertyDescriptor['isGatewayServiceWebSocket'] = true;
        testPropertyDescriptor['isDebugActive'] = true;
        testPropertyDescriptor['webSocketClient'] = 'test client';
        testPropertyDescriptor['connectionHeaders'] = 'test headers';

        testPropertyDescriptor['value']()
        .then(() => {
            const disconnectParams = {
                webSocketClient: 'test client',
                connectionHeaders: 'test headers',
                isDebugActive: true,
                disconnectionCallback: 'mockDisconnectionCallback'
            };
            expect(testStubs[1].calledWith(disconnectParams)).to.equal(true);
            done();
        });
    });
});
