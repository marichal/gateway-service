import { expect } from 'chai';
import 'mocha';
import { GatewayService } from '../../../../lib/decorators/services/gateway-service';

describe('check GatewayService decorator', () => {

    it('check GatewayService constructor', () => {

        const testDecoratedObject = new (GatewayService('testUrl')(class TestClass {}));

        expect(testDecoratedObject.baseUrl).to.equal('testUrl');
        expect(testDecoratedObject.isGatewayService).to.equal(true);
    });
});
