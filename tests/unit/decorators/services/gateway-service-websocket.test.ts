import { expect } from 'chai';
import 'mocha';
import { GatewayServiceWebSocket } from '../../../../lib/decorators/services/gateway-service-websocket';

describe('check GatewayServiceWebSocket decorator', () => {

    it('check GatewayServiceWebSocket constructor', () => {

        const testDecoratedObject = new (GatewayServiceWebSocket('testUrl', true, { header: 'mockHeader' })(class TestClass {}));

        expect(testDecoratedObject.isGatewayServiceWebSocket).to.equal(true);
        expect(testDecoratedObject.isDebugActive).to.equal(true);
        expect(testDecoratedObject.connectionHeaders).to.deep.equal({ header: 'mockHeader' });
    });
});
