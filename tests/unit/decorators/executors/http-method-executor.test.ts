import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { HttpMethodExecutor } from '../../../../lib/decorators/executors/http-method-executor';
import { GatewayServiceError } from '../../../../lib/errors/gateway-service-error';
import { MetadataHelper } from '../../../../lib/helpers/metadata-helper';
import { HttpRequestHelper } from '../../../../lib/helpers/http-request-helper';
import { UrlHelper } from '../../../../lib/helpers/url-helper';

describe('check HttpMethodExecutor', () => {
	let httpMethodExecutor;
    let methodResolver;
    const testStubs = [];

	before(function() {

        httpMethodExecutor = new HttpMethodExecutor('testUrl');
        methodResolver = sinon.spy();

        testStubs.push(
            sinon.stub(UrlHelper, 'replacePathParameters').callsFake(() => 'mockUrl'),
            sinon.stub(UrlHelper, 'addQueryParameters').callsFake(() => 'mockUrl'),
            sinon.stub(MetadataHelper, 'getMappingMetadata').callsFake(() => 'mockMapper'),
            sinon.stub(HttpRequestHelper, 'getBodyObject').callsFake(() => 'mockBodyObject')
        );
	});

	after(function() {

        testStubs.forEach((stub) => { stub.restore(); });
    });

    it('check constructor', () => {

        expect(httpMethodExecutor.url).to.be.equal('testUrl');
    });

    it('check execute method on a non GatewayService object', (done) => {

        const returnFunction = httpMethodExecutor.execute(methodResolver);

        const testPropertyDescriptor  = {};
        returnFunction('testTarget', 'testPropertyKey', testPropertyDescriptor);

        testPropertyDescriptor['value']()
        .catch((error) => {
            expect(error instanceof GatewayServiceError);
            done();
        });
    });

    it('check execute method on a GatewayService object', () => {

        const returnFunction = httpMethodExecutor.execute(methodResolver);

        const testPropertyDescriptor  = {};
        returnFunction('testTarget', 'testPropertyKey', testPropertyDescriptor);

        testPropertyDescriptor['isGatewayService'] = true;
        testPropertyDescriptor['value']();

        const methodResolverParameters = { url: 'mockUrl', mapper: 'mockMapper', body: 'mockBodyObject' };
        expect(methodResolver.calledWith(methodResolverParameters)).to.equal(true);
	});
});
