import { expect } from 'chai';
import 'mocha';
import * as sinon from 'sinon';
import { WebSocketMethodExecutor } from '../../../../lib/decorators/executors/websocket-method-executor';
import { GatewayServiceError } from '../../../../lib/errors/gateway-service-error';
import { WebSocketHelper } from '../../../../lib/helpers/websocket-helper';

describe('check WebSockerMethodExecutor', () => {
	let webSocketMethodExecutor;
    let methodResolver;
    const testStubs = [];

	before(function() {

        webSocketMethodExecutor = new WebSocketMethodExecutor();
        methodResolver = sinon.spy();

        testStubs.push(
            sinon.stub(WebSocketHelper, 'getWebSocketMessage').callsFake(() => 'test message'),
            sinon.stub(WebSocketHelper, 'getSubscriptionCallback').callsFake(() => 'mockSubscriptionCallback'),
            sinon.stub(WebSocketHelper, 'getDisconnectionCallback').callsFake(() => 'mockDisconnectionCallback'),
        );
    });

	after(function() {

        testStubs.forEach((stub) => { stub.restore(); });
    });

    it('check execute method on a non GatewayServiceWebSocket object', (done) => {

        const returnFunction = webSocketMethodExecutor.execute(methodResolver);

        const testPropertyDescriptor  = {};
        returnFunction('testTarget', 'testPropertyKey', testPropertyDescriptor);

        testPropertyDescriptor['value']()
        .catch((error) => {
            expect(error instanceof GatewayServiceError);
            done();
        });
    });

    it('check execute method on a GatewayServiceWebSocket object', () => {

        const returnFunction = webSocketMethodExecutor.execute(methodResolver);

        const testPropertyDescriptor  = {};
        returnFunction('testTarget', 'testPropertyKey', testPropertyDescriptor);

        testPropertyDescriptor['isGatewayServiceWebSocket'] = true;
        testPropertyDescriptor['isDebugActive'] = true;
        testPropertyDescriptor['webSocketClient'] = 'test client';
        testPropertyDescriptor['connectionHeaders'] = 'test headers';
        testPropertyDescriptor['value']();

        const methodResolverParameters = {
            webSocketClient: 'test client',
            isDebugActive: true,
            connectionHeaders: 'test headers',
            outgoingMessage: 'test message',
            subscriptionCallback: 'mockSubscriptionCallback'
        };
        expect(methodResolver.calledWith(methodResolverParameters)).to.equal(true);
	});
});
