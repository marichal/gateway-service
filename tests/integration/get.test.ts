import { expect } from 'chai';
import 'mocha';
import express = require('express');
import { GatewayService, Get, Path, QueryParameters, Mapping } from '../../index';
import { Player } from './types/player';

@GatewayService('http://localhost:3000')
class Service {

    @Get('/players')
    @Mapping(httpResponse => httpResponse._embedded.players)
    getAllPlayers(): Promise<Array<Player>> { return null; }

    @Get('/players/${id}')
	getPlayerById(@Path('id') id: number): Promise<Player> { return null; }

	@Get('/playersWithQuery')
    getPlayerByFirstName(@QueryParameters parameters: object): Promise<Player> { return null; }
}

function buildHttpServer(app) {

	const players = [
		{ id: 1, firstName: 'Leo', lastName: 'Messi' },
		{ id: 2, firstName: 'Ousmane', lastName: 'Dembélé' },
		{ id: 3, firstName: 'Luis', lastName: 'Suárez' }
	];

	app.get('/players', (request, response) => {
		response.send({ _embedded: { players: players } });
	});

	app.get('/players/:id', (request, response) => {
		response.send(players[request.params.id]);
	});

	app.get('/playersWithQuery', (request, response) => {
		const playerIndex = players.findIndex((player) => player.firstName === request.query.name);
		response.send(players[playerIndex]);
	});

	return app.listen(3000);
}

describe('check GET requests', () => {
	const testApp = express();
	let testHttpServer;
	const testService = new Service();

	before(function() {

		testHttpServer = buildHttpServer(testApp);
	});

	after(function() {

		testHttpServer.close();
	});

	it('check GET with mapping', () => {

		return testService.getAllPlayers()
		.then((players: Array<Player>) => {
			expect(players.length).to.be.equal(3);
			expect(players[0]).to.deep.equal({ id: 1, firstName: 'Leo', lastName: 'Messi' });
			expect(players[1]).to.deep.equal({ id: 2, firstName: 'Ousmane', lastName: 'Dembélé' });
			expect(players[2]).to.deep.equal({ id: 3, firstName: 'Luis', lastName: 'Suárez' });
		});
	});

	it('check GET with path parameter', () => {

		return testService.getPlayerById(1)
		.then((player: Player) => {
			expect(player).to.deep.equal({ id: 2, firstName: 'Ousmane', lastName: 'Dembélé' });
		});
	});

	it('check GET with query parameters', () => {

		return testService.getPlayerByFirstName({ name: 'Leo' })
		.then((player: Player) => {
			expect(player).to.deep.equal({ id: 1, firstName: 'Leo', lastName: 'Messi' });
		});
	});
});
