import { expect } from 'chai';
import 'mocha';
import express = require('express');
import bodyParser = require('body-parser');
import { GatewayService, Post, Patch, Put, Delete, Path, Body } from '../../index';
import { Player } from './types/player';

@GatewayService('http://localhost:3000')
class Service {

    @Post('/players')
    addPlayer(@Body league: Object): Promise<Player> { return null; }

    @Patch('/players/${id}')
    updatePlayer(@Path('id') id: number, @Body league: Object): Promise<Player> { return null; }

    @Delete('/players/${id}')
    deletePlayer(@Path('id') id: number): Promise<any> { return null; }
}

function buildHttpServer(app, database) {

	app.use(bodyParser.json());

	app.post('/players', (request, response) => {
		const newPLayer = request.body;
		database.push(newPLayer);
		response.status(201);
		response.send(newPLayer);
	});

	app.patch('/players/:id', (request, response) => {
		const playerId = parseInt(request.params.id, 10);
		const playerIndex = database.findIndex((player) => player.id === playerId);
		const updatedPlayer = database[playerIndex];
		const updatedAttributes = request.body;

		for (const property in updatedAttributes) {
			if (updatedAttributes.hasOwnProperty(property)) {
				updatedPlayer[property] = updatedAttributes[property];
			}
		}

		response.status(204);
		response.send(updatedPlayer);
	});

	app.delete('/players/:id', (request, response) => {
		const playerId = parseInt(request.params.id, 10);
		const playerIndex = database.findIndex((player) => player.id === playerId);
		database.splice(playerIndex, 1);

		response.status(204);
		response.send(null);
	});

	return app.listen(3000);
}

function buildTestDatabase() {

	return [
		{ id: 1, firstName: 'Leo', lastName: 'Messi' },
		{ id: 2, firstName: 'Ousmane', lastName: 'Dembélé' },
		{ id: 3, firstName: 'Luis', lastName: 'Suárez' }
	];
}

describe('check updating requests', () => {
	const testApp = express();
	let testHttpServer;
	const testService = new Service();
	let testDatabase;

	before(function() {

		testDatabase = buildTestDatabase();
		testHttpServer = buildHttpServer(testApp, testDatabase);
	});

	after(function() {

		testHttpServer.close();
	});

	it('check POST request', () => {

		return testService.addPlayer({ id: 4, firstName: 'Ter', lastName: 'Stegen' })
		.then((player: Player) => {
			expect(testDatabase.length).to.be.equal(4);
			expect(testDatabase[0]).to.deep.equal({ id: 1, firstName: 'Leo', lastName: 'Messi' });
			expect(testDatabase[1]).to.deep.equal({ id: 2, firstName: 'Ousmane', lastName: 'Dembélé' });
			expect(testDatabase[2]).to.deep.equal({ id: 3, firstName: 'Luis', lastName: 'Suárez' });
			expect(testDatabase[3]).to.deep.equal(player);
		});
	});

	it('check PATCH request', () => {

		return testService.updatePlayer(3, { firstName: 'Luisito' })
		.then(() => {
			expect(testDatabase.length).to.be.equal(4);
			expect(testDatabase[2]).to.deep.equal({ id: 3, firstName: 'Luisito', lastName: 'Suárez' });
		});
	});

	it('check DELETE request', () => {

		return testService.deletePlayer(2)
		.then(() => {
			expect(testDatabase.length).to.be.equal(3);
			expect(testDatabase[0]).to.deep.equal({ id: 1, firstName: 'Leo', lastName: 'Messi' });
			expect(testDatabase[1]).to.deep.equal({ id: 3, firstName: 'Luisito', lastName: 'Suárez' });
			expect(testDatabase[2]).to.deep.equal({ id: 4, firstName: 'Ter', lastName: 'Stegen' });
		});
	});
});
