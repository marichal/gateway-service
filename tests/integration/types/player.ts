export interface Player {

    eaId: number;
    firstName: string;
    lastName: string;
    leagueId: number;
}
