# gateway-service 0.5.0 (June 24, 2018)
* WebSocket support
  - @GatewayServiceWebSocket decorator
  - @SendMessage, @Subscribe, @DisconnectWebSocket decorators
  - @MessageBody, @SubscriptionCallback, @DisconnectionCallback

# gateway-service 0.1.0 (June 20, 2018)
* Minor improvements
  - Unit tests
  - Integration tests
  - Bitbucket pipeline

# gateway-service 0.0.1 (June 18, 2018)
* Initial release
  - @GatewayService decorator
  - @Get, @Post, @Put, @Patch, @Delete decorators
  - @Path, @QueryParameters, @Mapping
  - GatewayServiceError
