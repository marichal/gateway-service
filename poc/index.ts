import { Service } from './service';
import { League } from './league';
import { GatewayServiceError } from '..';
import * as fs from 'fs';

const service: Service = new Service();

// Samples in action...
getAllLeagues();
getLeagueById(1);
getPdfFile();
addLeague({ eaId: 80, name: 'New League', shortName: 'NLX' });
updateLeague(1, { name: 'Updated League' });
replaceLeague(1, { eaId: 53, name: 'Replaced Leagues', shortName: 'RL1' });
deleteLeague(20);

function getAllLeagues() {
    service.get()
    .then((leagues: Array<League>) => {
        if (leagues) {
            leagues.forEach((league: League) => { console.log(`${league.name} - ${league.shortName}`); });
        } else { console.log('No content.'); }
    }).catch((error) => { displayError(error); });
}

function getLeagueById(id: number) {
    service.getWithArgs(id)
    .then((league: League) => {
        if (league) { console.log(`${league.name} - ${league.shortName}`);
        } else { console.log('No content.'); }
    }).catch((error) => { displayError(error); });
}

function getPdfFile() {
    service.getPDF()
    .then((pdfFile: any) => {
        if (pdfFile) {
            fs.writeFileSync('/Users/manu/Desktop/sample.pdf', pdfFile);
        } else { console.log('No content.'); }
    }).catch((error) => { displayError(error); });
}

function addLeague(league: object) {
    service.post(league)
    .then((newLeague: League) => {
        if (newLeague) { console.log(`${newLeague.name} - ${newLeague.shortName}`);
        } else { console.log('No content.'); }
    }).catch((error) => { displayError(error); });
}

function updateLeague(id: number, league: object) {
    service.patch(id, league)
    .then((newLeague: League) => {
        if (newLeague) { console.log(`${newLeague.name} - ${newLeague.shortName}`);
        } else { console.log('No content.'); }
    }).catch((error) => { displayError(error); });
}

function replaceLeague(id: number, league: League) {
    service.put(id, league)
    .then((newLeague: League) => {
        if (newLeague) { console.log(`${newLeague.name} - ${newLeague.shortName}`);
        } else { console.log('No content.'); }
    }).catch((error) => { displayError(error); });
}

function deleteLeague(id: number) {
    service.delete(id)
    .then((response: any) => {
        if (response) { console.log(response);
        } else { console.log('No content.'); }
    }).catch((error) => { displayError(error); });
}

function displayError(error) {
    if (error instanceof GatewayServiceError) {
        console.log(JSON.stringify(error));
    } else {
        console.log(error);
    }
}
