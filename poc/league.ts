export interface League {

    eaId: number;
    name: string;
    shortName: string;
}
