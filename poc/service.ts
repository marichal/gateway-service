import { GatewayService, Get, Post, Patch, Put, Path, Delete, QueryParameters, Mapping, Body } from '..';
import { League } from './league';

@GatewayService('http://localhost:8080')
export class Service {

    // A simple GET request
    @Get('/leagues')
    @Mapping(httpResponse => httpResponse._embedded.leagues)
    get(): Promise<Array<League>> { return null; }

    // A GET request with params
    @Get('/leagues/${id}')
    getWithArgs(@Path('id') id: number): Promise<League> { return null; }

    // A GET request with params
    @Get('/pdf')
    getPDF(): Promise<any> { return null; }

    // A POST request
    @Post('/leagues')
    post(@Body league: Object): Promise<League> { return null; }

    // A PATCH request
    @Patch('/leagues/${id}')
    patch(@Path('id') id: number, @Body league: Object): Promise<League> { return null; }

    // A PUT request
    @Put('/leagues/${id}')
    put(@Path('id') id: number, @Body league: League): Promise<League> { return null; }

    // A DELETE request
    @Delete('/leagues/${id}')
    delete(@Path('id') id: number): Promise<any> { return null; }
}
