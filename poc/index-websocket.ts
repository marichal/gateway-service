import { WebSockectService } from './websocket-service';
import { GatewayServiceError } from '..';

const service: WebSockectService = new WebSockectService();

// Samples in action...
setTimeout(() => { subscribe(); } , 3000);
setTimeout(() => { sendMessage(); } , 6000);
setTimeout(() => { disconnect(); } , 9000);

function subscribe() {
    service.subscribe((message: string) => {
        console.log(`INCOMING MESSAGE: ${message}`);
    })
    .then(() => {
        console.log('Successfully subscribed');
    }).catch((error) => { displayError(error); });
}

function sendMessage() {
    service.sendMessage('{ message: 1 }')
    .then(() => {
        console.log('Message Successfully delivered');
    }).catch((error) => { displayError(error); });
}

function disconnect() {
    service.disconnect(() => {
        console.log(`DISCONNECTED`);
    })
    .then(() => {})
    .catch((error) => { displayError(error); });
}

function displayError(error) {
    console.log('HANDLED ERROR');
    if (error instanceof GatewayServiceError) {
        console.log(JSON.stringify(error));
    } else {
        console.log(error);
    }
}
