import { GatewayServiceWebSocket } from '..';
import { SendMessage } from '../lib/decorators/websocket-methods/send-message';
import { MessageBody } from '../lib/decorators/websocket-modifiers/message-body';
import { Subscribe } from '../lib/decorators/websocket-methods/subscribe';
import { SubscriptionCallback } from '../lib/decorators/websocket-modifiers/subscription-callback';
import { DisconnectWebSocket } from '../lib/decorators/websocket-methods/disconnect-websocket';
import { DisconnectionCallback } from '../lib/decorators/websocket-modifiers/disconnection-callback';

@GatewayServiceWebSocket('http://localhost:8080/demo-springboot', true)
export class WebSockectService {

    @SendMessage('/app/hello')
    sendMessage(@MessageBody body: string): Promise<any> { return null; }

    @Subscribe('/topic/greetings')
    subscribe(@SubscriptionCallback callback: Function): Promise<any> { return null; }

    @DisconnectWebSocket
    disconnect(@DisconnectionCallback callback: () => any): Promise<any> { return null; }
}
