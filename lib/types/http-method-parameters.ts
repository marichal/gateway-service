export interface HttpMethodParameters {

    url: string;
    mapper?: Function;
    body?: Object;
}
