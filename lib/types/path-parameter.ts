export interface PathParameter {

    parameterIndex: number;
    identifier: string;
}
