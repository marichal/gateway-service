import { Client, StompHeaders, Message } from '@stomp/stompjs';

export interface WebSocketMethodParameters {

    webSocketClient: Client;
    isDebugActive: boolean;
    connectionHeaders: StompHeaders;
    outgoingMessage?: string;
    subscriptionCallback?: Function;
    disconnectionCallback?: Function;
}
