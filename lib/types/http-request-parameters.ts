import { HttpMethodParameters } from './http-method-parameters';

export interface HttpRequestParameters {

    method: string;
    httpMethodParameters: HttpMethodParameters;
    expectedStatusCodes: Array<number>;
    expectedContentTypes?: Array<string>;
}
