export interface MethodExecutionDescriptor {

    target: any;
    propertyKey: string;
    arguments: IArguments;
}
