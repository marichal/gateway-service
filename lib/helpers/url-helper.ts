import * as Http from 'http';
import { PathParameter } from '../types/path-parameter';
import { MethodExecutionDescriptor } from '../types/method-execution-descriptor';
import { MetadataHelper } from './metadata-helper';

export class UrlHelper {

    static replacePathParameters(url: string, methodExecutionDescription: MethodExecutionDescriptor): string {

        let urlWithPathParameters = url;

        const pathParameters: PathParameter[] = MetadataHelper.getPathParamsMetadatata(methodExecutionDescription) || [];
        pathParameters.forEach((pathParameter) => {

            urlWithPathParameters =
                urlWithPathParameters.replace(new RegExp(`[$][{]${pathParameter.identifier}[}]`, 'g'),
                                              methodExecutionDescription.arguments[pathParameter.parameterIndex]);
        });

        if (urlWithPathParameters.indexOf('${') !== -1) {

            throw new Error(`Missing path parameter: ${urlWithPathParameters} (method ${methodExecutionDescription.propertyKey})`);
        }

        return urlWithPathParameters;
    }

    static addQueryParameters(url: string, methodExecutionDescription: MethodExecutionDescriptor): string {

        let urlWithQueryParameters = url;

        const queryParameterIndex: number = MetadataHelper.getQueryParamIndexMetadata(methodExecutionDescription);
        if (queryParameterIndex !== undefined) {

            const queryParameters = methodExecutionDescription.arguments[queryParameterIndex];
            if (typeof queryParameters !== 'object') {

                throw new Error(`@QueryParameters must be Objects (method '${methodExecutionDescription.propertyKey}')`);
            }

            let queryString = '';
            let currentQuery;
            for (const property in queryParameters) {
                if (queryParameters.hasOwnProperty(property)) {
                    currentQuery = `${property}=${queryParameters[property]}`;
                    queryString += ((queryString === '') ? `?${currentQuery}` : `&${currentQuery}`);
                }
            }

            urlWithQueryParameters += queryString;
        }

        return urlWithQueryParameters;
    }

    static extractUrlComponents (url: string): Http.RequestOptions {

        const httpRequestOptions: Http.RequestOptions = {

            hostname: '',
            port: 80,
            path: '/',
            method: '',
            headers: {}
        };

        // Extract protocol
        const urlComponents = url.split('//');
        let hostAndPath = urlComponents[0];
        if (urlComponents.length > 1) {
            hostAndPath = urlComponents[1];
        }

        // Extract path
        const hostAndPortIndex = hostAndPath.indexOf('/');
        let host;
        if (hostAndPortIndex !== -1) {

            httpRequestOptions.path = hostAndPath.substring(hostAndPortIndex);
            host = hostAndPath.substring(0, hostAndPortIndex);

        } else {

            host = hostAndPath;
        }

        // Extract host and port
        const hostAndPort = host.split(':');
        if (hostAndPort.length > 1) {

            httpRequestOptions.port = parseInt(hostAndPort[1], 10);
        }

        httpRequestOptions.hostname = hostAndPort[0];

        return httpRequestOptions;
    }
}
