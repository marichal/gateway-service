import { Client, StompHeaders, Message } from '@stomp/stompjs';
import { MethodExecutionDescriptor } from '../types/method-execution-descriptor';
import { MetadataHelper } from './metadata-helper';
import { WebSocketMethodParameters } from '../types/websocket-method-parameters';
import { GatewayServiceErrorHelper } from './gateway-service-error-helper';

export class WebSocketHelper {

    static getWebSocketMessage(methodExecutionDescriptor: MethodExecutionDescriptor): string {

        let messageParameter = null;

		const messageParameterIndex: number = MetadataHelper.getMessageParamIndexMetadata(methodExecutionDescriptor);
        if (messageParameterIndex !== undefined) {

			messageParameter = methodExecutionDescriptor.arguments[messageParameterIndex];

            if (typeof messageParameter !== 'string') {

                throw new Error(`@Message parameter must be of type string (method '${methodExecutionDescriptor.propertyKey}')`);
            }
        }

        return messageParameter;
    }

    static getSubscriptionCallback(methodExecutionDescriptor: MethodExecutionDescriptor): Function {

        let subscriptionCallback = null;

		const subscriptionCallbackIndex: number = MetadataHelper.getSubscriptionCallbackParamIndexMetadata(methodExecutionDescriptor);
        if (subscriptionCallbackIndex !== undefined) {

			subscriptionCallback = methodExecutionDescriptor.arguments[subscriptionCallbackIndex];

            if (typeof subscriptionCallback !== 'function') {

                throw new Error(`@SubscriptionCallback parameter must be a function (method '${methodExecutionDescriptor.propertyKey}')`);
            }
        }

        return subscriptionCallback;
    }

    static getDisconnectionCallback(methodExecutionDescriptor: MethodExecutionDescriptor): Function {

        let disconnectionCallback = null;

		const disconnectionCallbackIndex: number = MetadataHelper.getDisconnectionCallbackParamIndexMetadata(methodExecutionDescriptor);
        if (disconnectionCallbackIndex !== undefined) {

			disconnectionCallback = methodExecutionDescriptor.arguments[disconnectionCallbackIndex];

            if (typeof disconnectionCallback !== 'function') {

                throw new Error(`@DisconnectionCallback parameter must be a function (method '${methodExecutionDescriptor.propertyKey}')`);
            }
        }

        return disconnectionCallback;
    }

    static sendMessage(webSocketMethodParameters: WebSocketMethodParameters, destination: string): Promise<void> {

        return new Promise((resolve: Function, reject: Function) => {

            const webSocketClient = webSocketMethodParameters.webSocketClient;
            this._checkConnection(webSocketClient, webSocketMethodParameters.isDebugActive, webSocketMethodParameters.connectionHeaders)
            .then(() => {

                webSocketClient.send(destination, {}, webSocketMethodParameters.outgoingMessage);

                return resolve();
            })
            .catch((error) => {

                return GatewayServiceErrorHelper.prepareHttpErrorAndReject(error, `WebSocket endpoint: ${destination}`, reject);
            });
        });
    }

    static subscribeToDestination(webSocketMethodParameters: WebSocketMethodParameters, destination: string): Promise<void> {

        return new Promise((resolve: Function, reject: Function) => {

            const webSocketClient = webSocketMethodParameters.webSocketClient;
            this._checkConnection(webSocketClient, webSocketMethodParameters.isDebugActive, webSocketMethodParameters.connectionHeaders)
            .then(() => {

                webSocketClient.subscribe(destination, (message: Message) => {

                    webSocketMethodParameters.subscriptionCallback(message.body);
                }, {});

                return resolve();
            })
            .catch((error) => {

                return GatewayServiceErrorHelper.prepareHttpErrorAndReject(error, `WebSocket endpoint: ${destination}`, reject);
            });
        });
    }

    static disconnect(webSocketMethodParameters: WebSocketMethodParameters): Promise<void> {

        return new Promise((resolve: Function, reject: Function) => {

            try {

                const webSocketClient = webSocketMethodParameters.webSocketClient;
                if (webSocketClient.connected) {

                    webSocketClient.disconnect(() => { webSocketMethodParameters.disconnectionCallback(); });
                }

                return resolve();

            } catch (error) {

                return reject(error);
            }
        });
    }

    static _checkConnection(webSocketClient: Client, withDebug: boolean, headers: StompHeaders): Promise<any> {

        return new Promise((resolve: Function, reject: Function) => {

            if (webSocketClient.connected) {

                return resolve();

            } else {

                if (withDebug) {

                    webSocketClient.debug = function(str) { console.log(`STOMPJS: ${str}`); };
                }

                webSocketClient.connect(headers || {}, () => resolve(), (error) => reject(error));
            }
        });
    }
}
