import { GatewayServiceError } from '../errors/gateway-service-error';
import { HttpRequestParameters } from '../types/http-request-parameters';
import { HttpRequestHelper } from './http-request-helper';

export class HttpResponseHelper {

    static processHttpResponse(httpResponse: any, httpRequestParameters: HttpRequestParameters , callback: Function): void {

        const url = httpRequestParameters.httpMethodParameters.url;
        const contentType = httpResponse.headers['content-type'];

        this._checkContenType(url, contentType, httpRequestParameters.expectedContentTypes);

        this._setEncondingAccordingToContentType(contentType, httpResponse);

        const rawData = [];

        httpResponse.on('data', (chunk) => { rawData.push(new Buffer(chunk)); });

        httpResponse.on('end', () => {

            const location = httpResponse.headers['location'];
            if (location) {
                return this._getResourceFromLocation(location, callback);
            }

            if (httpRequestParameters.expectedStatusCodes.indexOf(httpResponse.statusCode) === -1) {

                const responseAsErrorObject = (rawData.length > 0 ? JSON.parse(Buffer.concat(rawData).toString()) : {});

                return callback(
                    new GatewayServiceError.Builder(responseAsErrorObject.message || 'Unexpected status code')
                        .withError(responseAsErrorObject.exception)
                        .withCause(responseAsErrorObject.cause)
                        .withStatusCode(httpResponse.statusCode)
                        .withPath(url)
                        .withStackTrace(responseAsErrorObject.stackTrace)
                        .build()
                );
            }

            return callback(null, this._buildResponseAccordingToContentType(contentType, Buffer.concat(rawData)));
        });
    }

    static _checkContenType(url: string, contentType: string, expectedContentTypes: Array<string>): void {

        if (contentType && expectedContentTypes) {

            const isThereAnyExpectedContentTypes =
                expectedContentTypes.some((expectedContentType) => contentType.indexOf(expectedContentType) !== -1);

            if (isThereAnyExpectedContentTypes === false) {

                throw new GatewayServiceError.Builder(`Unexpected content type: ${contentType}`)
                    .withPath(url)
                    .build();
            }
        }
    }

    static _getResourceFromLocation(location: string, callback: Function): void {

        const httpRequestParameters: HttpRequestParameters = {
            method: 'GET',
            httpMethodParameters: { url: location },
            expectedContentTypes: ['application/json', 'application/hal+json', 'application/pdf'],
            expectedStatusCodes: [200],
        };

        HttpRequestHelper.doRequest(httpRequestParameters)
        .then((response) => {

            callback(null, response);
        })
        .catch((error) => {

            callback(error);
        });
    }

    static _setEncondingAccordingToContentType(contentType: string, httpResponse: any) {

        if (contentType && contentType.indexOf('application/pdf') === -1) {
            httpResponse.setEncoding('utf8');
        }
    }

    static _buildResponseAccordingToContentType(contentType: string, rawResponse: Buffer): any {

        if (rawResponse.byteLength === 0) {

            return null;
        }

        if (!contentType || contentType.indexOf('application/json') !== -1 || contentType.indexOf('application/hal+json') !== -1) {

            return JSON.parse(rawResponse.toString());

        } else if (contentType.indexOf('application/pdf') !== -1) {

            return rawResponse;
        }

        return rawResponse;
    }
}
