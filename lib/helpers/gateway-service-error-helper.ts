import { GatewayServiceError } from '../errors/gateway-service-error';

export class GatewayServiceErrorHelper {

    static prepareHttpErrorAndReject(error: string | Error | GatewayServiceError, url: string, reject: Function): void {

        if (error instanceof GatewayServiceError) {

            return reject(error);

        } else {

            let errorMessage = error;
            let stackTraceLines = [];
            if (error instanceof Error) {

                errorMessage = error.name + ': ' + error.message;
                stackTraceLines = error.stack.split('\n');
            }

            return reject(
                new GatewayServiceError.Builder(errorMessage)
                    .withStatusCode(500)
                    .withCause()
                    .withPath(url)
                    .withStackTrace(stackTraceLines)
                    .build());
        }
    }
}
