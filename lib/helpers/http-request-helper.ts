import * as Http from 'http';
import { UrlHelper } from './url-helper';
import { GatewayServiceErrorHelper } from './gateway-service-error-helper';
import { HttpResponseHelper } from './http-response-helper';
import { MethodExecutionDescriptor } from '../types/method-execution-descriptor';
import { MetadataHelper } from './metadata-helper';
import { HttpRequestParameters } from '../types/http-request-parameters';
import { GatewayServiceError } from '../..';

export class HttpRequestHelper {

	static doRequest(httpRequestParameters: HttpRequestParameters): Promise<any> {

		return new Promise((resolve: Function, reject: Function) => {

			const url = httpRequestParameters.httpMethodParameters.url;
			const requestOptions: Http.RequestOptions = UrlHelper.extractUrlComponents(url);
			requestOptions.method = httpRequestParameters.method;

			const bodyObject = httpRequestParameters.httpMethodParameters.body;
			if (bodyObject) {

				requestOptions.headers = { 'Content-Type': 'application/json' };
			}

			const httpRequest = Http.request(requestOptions, (httpResponse) => {

				try {

					HttpResponseHelper.processHttpResponse(httpResponse, httpRequestParameters, (error, response) => {

						if (error) {

							return this._handleErrorAndReject(error, httpRequestParameters.method, url, resolve, reject);

						} else {

							const mappingFunction = httpRequestParameters.httpMethodParameters.mapper;
							if (mappingFunction && response) {

								response = mappingFunction(response);
							}

							return resolve(response);
						}
					});

				} catch (error) {

					httpResponse.resume();

					return this._handleErrorAndReject(error, httpRequestParameters.method, url, resolve, reject);
				}
			});

			httpRequest.on('error', (error) => GatewayServiceErrorHelper.prepareHttpErrorAndReject(error.message, url, reject));

			httpRequest.end(bodyObject ? JSON.stringify(bodyObject) : null);
		});
	}

	static getBodyObject(methodExecutionDescriptor: MethodExecutionDescriptor): Object {

        let bodyParameter = null;

		const bodyParameterIndex: number = MetadataHelper.getBodyParamIndexMetadata(methodExecutionDescriptor);
        if (bodyParameterIndex !== undefined) {

			bodyParameter = methodExecutionDescriptor.arguments[bodyParameterIndex];

            if (typeof bodyParameter !== 'object') {

                throw new Error(`@Body parameter must be an Object (method '${methodExecutionDescriptor.propertyKey}')`);
            }
        }

        return bodyParameter;
	}

	static _handleErrorAndReject(error: string | GatewayServiceError, method: string, url: string, resolve: Function, reject: Function): void {

		if (method.toUpperCase() !== 'GET') {

			return GatewayServiceErrorHelper.prepareHttpErrorAndReject(error, url, reject);
		}

		return this._handleHttpStatus404(error, url, resolve, reject);
	}

	static _handleHttpStatus404(error: string | GatewayServiceError, url: string, resolve: Function, reject: Function): void {

		if (error instanceof GatewayServiceError && error.getStatusCode() === 404) {

			return resolve(null);

		} else {

			return GatewayServiceErrorHelper.prepareHttpErrorAndReject(error, url, reject);
		}
	}
}
