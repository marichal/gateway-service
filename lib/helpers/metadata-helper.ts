import 'reflect-metadata';
import { MethodExecutionDescriptor } from '../types/method-execution-descriptor';
import { PathParameter } from '../types/path-parameter';

const pathMetadataKey = Symbol('Path');
const queryParamsMetadataKey = Symbol('QueryParameters');
const mappingMetadataKey = Symbol('Mapping');
const bodyParamMetadataKey = Symbol('Body');
const messageMetadataKey = Symbol('Message');
const subscriptionCallbackMetadataKey = Symbol('SubsCallback');
const disconnectionCallbackMetadataKey = Symbol('DisconnectCallback');

export class MetadataHelper {

    static setPathParamsMetadata(methodExecutionDescriptor: MethodExecutionDescriptor, value: PathParameter[]): void {

        Reflect.defineMetadata(pathMetadataKey, value, methodExecutionDescriptor.target, methodExecutionDescriptor.propertyKey);
    }

    static getPathParamsMetadatata(methodExecutionDescriptor: MethodExecutionDescriptor): PathParameter[] {

        return Reflect.getOwnMetadata(pathMetadataKey, methodExecutionDescriptor.target, methodExecutionDescriptor.propertyKey);
    }

    static setQueryParamIndexMetadata(methodExecutionDescriptor: MethodExecutionDescriptor, value: number): void {

        Reflect.defineMetadata(queryParamsMetadataKey, value, methodExecutionDescriptor.target, methodExecutionDescriptor.propertyKey);
    }

    static getQueryParamIndexMetadata(methodExecutionDescriptor: MethodExecutionDescriptor): number {

        return Reflect.getOwnMetadata(queryParamsMetadataKey, methodExecutionDescriptor.target, methodExecutionDescriptor.propertyKey);
    }

    static setMappingMetadata(methodExecutionDescriptor: MethodExecutionDescriptor, value: Function): void {

        Reflect.defineMetadata(mappingMetadataKey, value, methodExecutionDescriptor.target, methodExecutionDescriptor.propertyKey);
    }

    static getMappingMetadata(methodExecutionDescriptor: MethodExecutionDescriptor): Function {

        return Reflect.getOwnMetadata(mappingMetadataKey, methodExecutionDescriptor.target, methodExecutionDescriptor.propertyKey);
    }

    static setBodyParamIndexMetadata(methodExecutionDescriptor: MethodExecutionDescriptor, value: number): void {

        Reflect.defineMetadata(bodyParamMetadataKey, value, methodExecutionDescriptor.target, methodExecutionDescriptor.propertyKey);
    }

    static getBodyParamIndexMetadata(methodExecutionDescriptor: MethodExecutionDescriptor): number {

        return Reflect.getOwnMetadata(bodyParamMetadataKey, methodExecutionDescriptor.target, methodExecutionDescriptor.propertyKey);
    }

    static setMessageParamIndexMetadata(methodExecutionDescriptor: MethodExecutionDescriptor, value: number): void {

        Reflect.defineMetadata(messageMetadataKey, value, methodExecutionDescriptor.target, methodExecutionDescriptor.propertyKey);
    }

    static getMessageParamIndexMetadata(methodExecutionDescriptor: MethodExecutionDescriptor): number {

        return Reflect.getOwnMetadata(messageMetadataKey, methodExecutionDescriptor.target, methodExecutionDescriptor.propertyKey);
    }

    static setSubscriptionCallbackParamIndexMetadata(methodExecutionDescriptor: MethodExecutionDescriptor, value: number): void {

        Reflect.defineMetadata(subscriptionCallbackMetadataKey, value, methodExecutionDescriptor.target,
            methodExecutionDescriptor.propertyKey);
    }

    static getSubscriptionCallbackParamIndexMetadata(methodExecutionDescriptor: MethodExecutionDescriptor): number {

        return Reflect.getOwnMetadata(subscriptionCallbackMetadataKey, methodExecutionDescriptor.target,
            methodExecutionDescriptor.propertyKey);
    }

    static setDisconnectionCallbackParamIndexMetadata(methodExecutionDescriptor: MethodExecutionDescriptor, value: number): void {

        Reflect.defineMetadata(disconnectionCallbackMetadataKey, value, methodExecutionDescriptor.target,
            methodExecutionDescriptor.propertyKey);
    }

    static getDisconnectionCallbackParamIndexMetadata(methodExecutionDescriptor: MethodExecutionDescriptor): number {

        return Reflect.getOwnMetadata(disconnectionCallbackMetadataKey, methodExecutionDescriptor.target,
            methodExecutionDescriptor.propertyKey);
    }
}
