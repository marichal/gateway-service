export class GatewayServiceError  {

    private error: string;
    private message: string;
    private cause: string;
    private statusCode: number;
    private path: string;
    private stackTrace: Array<string>;
    private timeStamp: number;

    constructor(builder: any) {

        this.error = builder.error;
        this.message = builder.message;
        this.cause = builder.cause;
        this.statusCode = builder.statusCode;
        this.path = builder.path;
        this.stackTrace = builder.stackTrace;
        this.timeStamp = Date.now();
    }

    getError(): string {

        return this.error;
    }

    getMessage(): string {

        return this.message;
    }

    getCause(): string {

        return this.cause;
    }

    getStatusCode(): number {

        return this.statusCode;
    }

    getPath(): string {

        return this.path;
    }

    getStackTrace(): Array<string> {

        return this.stackTrace;
    }

    getTimeStamp(): number {

        return this.timeStamp;
    }

    static get Builder(): any {

        class Builder {

            private error: string;
            private message: string;
            private cause: string;
            private statusCode: number;
            private path: string;
            private stackTrace: Array<string>;

            constructor(message: string) {

                this.error = '';
                this.message = message;
                this.cause = '';
                this.statusCode = 500;
                this.path = '';
                this.stackTrace = [];
            }

            withError(error: string): Builder {

                this.error = error;
                return this;
            }

            withCause(cause: string): Builder {

                this.cause = cause;
                return this;
            }

            withStatusCode(statusCode: number): Builder {

                this.statusCode = statusCode;
                return this;
            }

            withPath(path: string): Builder {

                this.path = path;
                return this;
            }

            withStackTrace(stackTrace: Array<string>): Builder {

                this.stackTrace = stackTrace;
                return this;
            }

            build(): GatewayServiceError {

                return new GatewayServiceError(this);
            }
        }

        return Builder;
    }
}
