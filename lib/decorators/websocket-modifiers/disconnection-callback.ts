import { MetadataHelper } from '../../helpers/metadata-helper';
import { MethodExecutionDescriptor } from '../../types/method-execution-descriptor';

export function DisconnectionCallback (target: Object, propertyKey: string, parameterIndex: number): void {

    const methodExecutionDescriptor: MethodExecutionDescriptor = {
        target,
        propertyKey,
        arguments,
    };

    _addDisconnectionCallbackParameterInfoToMetadata(methodExecutionDescriptor, parameterIndex);
}

function _addDisconnectionCallbackParameterInfoToMetadata(
    methodExecutionDescriptor: MethodExecutionDescriptor,
    parameterIndex: number): void {

    const existingCallbackParameter: number = MetadataHelper.getDisconnectionCallbackParamIndexMetadata(methodExecutionDescriptor) || null;
    if (existingCallbackParameter !== null) {

        throw new Error(`Multiple @DisconnectionCallback for method '${methodExecutionDescriptor.propertyKey}'`);
    }

    MetadataHelper.setDisconnectionCallbackParamIndexMetadata(methodExecutionDescriptor, parameterIndex);
}
