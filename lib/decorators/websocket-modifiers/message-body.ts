import { MetadataHelper } from '../../helpers/metadata-helper';
import { MethodExecutionDescriptor } from '../../types/method-execution-descriptor';

export function MessageBody (target: Object, propertyKey: string, parameterIndex: number): void {

    const methodExecutionDescriptor: MethodExecutionDescriptor = {
        target,
        propertyKey,
        arguments,
    };

    _addMessageParameterInfoToMetadata(methodExecutionDescriptor, parameterIndex);
}

function _addMessageParameterInfoToMetadata(methodExecutionDescriptor: MethodExecutionDescriptor, parameterIndex: number): void {

    const existingMessageParameter: number = MetadataHelper.getMessageParamIndexMetadata(methodExecutionDescriptor) || null;
    if (existingMessageParameter !== null) {

        throw new Error(`Multiple @Message for method '${methodExecutionDescriptor.propertyKey}'`);
    }

    MetadataHelper.setMessageParamIndexMetadata(methodExecutionDescriptor, parameterIndex);
}
