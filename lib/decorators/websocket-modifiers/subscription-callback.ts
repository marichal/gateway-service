import { MetadataHelper } from '../../helpers/metadata-helper';
import { MethodExecutionDescriptor } from '../../types/method-execution-descriptor';

export function SubscriptionCallback (target: Object, propertyKey: string, parameterIndex: number): void {

    const methodExecutionDescriptor: MethodExecutionDescriptor = {
        target,
        propertyKey,
        arguments,
    };

    _addSubscriptionCallbackParameterInfoToMetadata(methodExecutionDescriptor, parameterIndex);
}

function _addSubscriptionCallbackParameterInfoToMetadata(
    methodExecutionDescriptor: MethodExecutionDescriptor,
    parameterIndex: number): void {

    const existingCallbackParameter: number = MetadataHelper.getSubscriptionCallbackParamIndexMetadata(methodExecutionDescriptor) || null;
    if (existingCallbackParameter !== null) {

        throw new Error(`Multiple @SubscriptionCallback for method '${methodExecutionDescriptor.propertyKey}'`);
    }

    MetadataHelper.setSubscriptionCallbackParamIndexMetadata(methodExecutionDescriptor, parameterIndex);
}
