import { HttpRequestHelper } from '../../helpers/http-request-helper';
import { HttpMethodExecutor } from '../executors/http-method-executor';
import { HttpMethodParameters } from '../../types/http-method-parameters';
import { HttpRequestParameters } from '../../types/http-request-parameters';

export function Get (url: string): Function {

    return new HttpMethodExecutor(url)
               .execute((httpMethodParameters: HttpMethodParameters) => {

        const httpRequestParameters: HttpRequestParameters = {
            method: 'GET',
            httpMethodParameters,
            expectedContentTypes: ['application/json', 'application/hal+json', 'application/pdf'],
            expectedStatusCodes: [200],
        };

        return HttpRequestHelper.doRequest(httpRequestParameters);
    });
}
