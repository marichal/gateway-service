import { HttpRequestHelper } from '../../helpers/http-request-helper';
import { HttpMethodExecutor } from '../executors/http-method-executor';
import { HttpMethodParameters } from '../../types/http-method-parameters';
import { HttpRequestParameters } from '../../types/http-request-parameters';

export function Post (url: string): Function {

    return new HttpMethodExecutor(url)
               .execute((httpMethodParameters: HttpMethodParameters) => {

        const httpRequestParameters: HttpRequestParameters = {
            method: 'POST',
            httpMethodParameters,
            expectedStatusCodes: [201],
        };

        return HttpRequestHelper.doRequest(httpRequestParameters);
    });
}
