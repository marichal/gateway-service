export function GatewayService (url: string): Function {

    return function <T extends { new(...args: any[]): {} }> (constructor: T) {

        return class extends constructor {

            baseUrl = url;
            isGatewayService = true;
        };
    };
}
