import { client, StompHeaders } from '@stomp/stompjs';

export function GatewayServiceWebSocket (url: string, withDebug?: boolean, headers?: StompHeaders): Function {

    return function <T extends { new(...args: any[]): {} }> (constructor: T) {

        return class extends constructor {

            isGatewayServiceWebSocket = true;
            isDebugActive =  withDebug || false;
            webSocketClient = client(url);
            connectionHeaders = headers || {};
        };
    };
}
