import { PathParameter } from '../../types/path-parameter';
import { MethodExecutionDescriptor } from '../../types/method-execution-descriptor';
import { MetadataHelper } from '../../helpers/metadata-helper';

export function Path (identifier: string): Function {

    return function (target: Object, propertyKey: string, parameterIndex: number): void {

        const methodExecutionDescriptor: MethodExecutionDescriptor = {
            target,
            propertyKey,
            arguments
        };

        _addPathParameterInfoToMetadata(methodExecutionDescriptor, parameterIndex, identifier);
    };
}

function _addPathParameterInfoToMetadata(methodExecutionDescriptor: MethodExecutionDescriptor,
                                        parameterIndex: number, identifier: string): void {

    const existingPathParameters: PathParameter[] = MetadataHelper.getPathParamsMetadatata(methodExecutionDescriptor) || [];
    existingPathParameters.push({
        parameterIndex,
        identifier
    });

    MetadataHelper.setPathParamsMetadata(methodExecutionDescriptor, existingPathParameters);
}
