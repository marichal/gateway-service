import { MetadataHelper } from '../../helpers/metadata-helper';
import { MethodExecutionDescriptor } from '../../types/method-execution-descriptor';

export function Body (target: Object, propertyKey: string, parameterIndex: number): void {

    const methodExecutionDescriptor: MethodExecutionDescriptor = {
        target,
        propertyKey,
        arguments,
    };

    _addBodyParameterInfoToMetadata(methodExecutionDescriptor, parameterIndex);
}

function _addBodyParameterInfoToMetadata(methodExecutionDescriptor: MethodExecutionDescriptor, parameterIndex: number): void {

    const existingBodyParameter: number = MetadataHelper.getBodyParamIndexMetadata(methodExecutionDescriptor) || null;
    if (existingBodyParameter !== null) {

        throw new Error(`Multiple @Body for method '${methodExecutionDescriptor.propertyKey}'`);
    }

    MetadataHelper.setBodyParamIndexMetadata(methodExecutionDescriptor, parameterIndex);
}
