import { MetadataHelper } from '../../helpers/metadata-helper';
import { MethodExecutionDescriptor } from '../../types/method-execution-descriptor';

export function Mapping (mappingFunction: Function): Function {

    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor): void {

        const methodExecutionDescriptor: MethodExecutionDescriptor = {
            target,
            propertyKey,
            arguments,
        };

        MetadataHelper.setMappingMetadata(methodExecutionDescriptor, mappingFunction);
    };
}
