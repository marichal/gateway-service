import { MethodExecutionDescriptor } from '../../types/method-execution-descriptor';
import { MetadataHelper } from '../../helpers/metadata-helper';

export function QueryParameters (target: Object, propertyKey: string, parameterIndex: number): void {

    const methodExecutionDescriptor: MethodExecutionDescriptor = {
        target,
        propertyKey,
        arguments,
    };

    _addQueryParameterInfoToMetadata(methodExecutionDescriptor, parameterIndex);
}

function _addQueryParameterInfoToMetadata(methodExecutionDescriptor: MethodExecutionDescriptor, parameterIndex: number): void {

    const existingQueryParameters: number = MetadataHelper.getQueryParamIndexMetadata(methodExecutionDescriptor) || null;
    if (existingQueryParameters !== null) {

        throw new Error(`Multiple @QueryParameters for method '${methodExecutionDescriptor.propertyKey}'`);
    }

    MetadataHelper.setQueryParamIndexMetadata(methodExecutionDescriptor, parameterIndex);
}
