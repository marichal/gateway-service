import { WebSocketMethodExecutor } from '../executors/websocket-method-executor';
import { WebSocketMethodParameters } from '../../types/websocket-method-parameters';
import { GatewayServiceError } from '../../errors/gateway-service-error';
import { WebSocketHelper } from '../../helpers/websocket-helper';

export function SendMessage (destination: string): Function {

    return new WebSocketMethodExecutor()
               .execute((webSocketMethodParameters: WebSocketMethodParameters) => {

        const message = webSocketMethodParameters.outgoingMessage;
        if (!message) {
            return Promise.reject(
                new GatewayServiceError.Builder('Missing @Message parameter in a @SendMessage method')
                .withStatusCode(500)
                .build()
            );
        }

        return WebSocketHelper.sendMessage(webSocketMethodParameters, destination);
    });
}
