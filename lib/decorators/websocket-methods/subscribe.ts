import { WebSocketMethodExecutor } from '../executors/websocket-method-executor';
import { WebSocketMethodParameters } from '../../types/websocket-method-parameters';
import { GatewayServiceError } from '../../errors/gateway-service-error';
import { WebSocketHelper } from '../../helpers/websocket-helper';

export function Subscribe (destination: string): Function {

    return new WebSocketMethodExecutor()
               .execute((webSocketMethodParameters: WebSocketMethodParameters) => {

        const subscriptionCallback = webSocketMethodParameters.subscriptionCallback;
        if (!subscriptionCallback) {
            return Promise.reject(
                new GatewayServiceError.Builder('Missing @SubscriptionCallback parameter in a @Subscribe method')
                .withStatusCode(500)
                .build()
            );
        }

        return WebSocketHelper.subscribeToDestination(webSocketMethodParameters, destination);
    });
}
