import { WebSocketMethodParameters } from '../../types/websocket-method-parameters';
import { GatewayServiceError } from '../../errors/gateway-service-error';
import { WebSocketHelper } from '../../helpers/websocket-helper';

export function DisconnectWebSocket (target: any, propertyKey: string, descriptor: PropertyDescriptor) {

    descriptor.value = function () {

        if (this['isGatewayServiceWebSocket'] !== true) {

            return Promise.reject(
                new GatewayServiceError.Builder('Not a @GatewayServiceWebSocket class')
                .withStatusCode(500)
                .build()
            );
        }

        const disconnectionCallback = WebSocketHelper.getDisconnectionCallback({target, propertyKey, arguments }) || null;
        if (!disconnectionCallback) {
            return Promise.reject(
                new GatewayServiceError.Builder('Missing @DisconnectionCallback parameter in a @DisconnectWebSocket method')
                .withStatusCode(500)
                .build()
            );
        }

        const webSocketMethodParameters: WebSocketMethodParameters = {
            webSocketClient: this['webSocketClient'],
            connectionHeaders: this['connectionHeaders'],
            isDebugActive: this['isDebugActive'],
            disconnectionCallback: disconnectionCallback
        };

        return WebSocketHelper.disconnect(webSocketMethodParameters);
    };
}
