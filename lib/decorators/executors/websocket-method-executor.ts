import { MethodExecutionDescriptor } from '../../types/method-execution-descriptor';
import { WebSocketHelper } from '../../helpers/websocket-helper';
import { WebSocketMethodParameters } from '../../types/websocket-method-parameters';
import { GatewayServiceError } from '../../errors/gateway-service-error';

export class WebSocketMethodExecutor  {

    execute(methodResolver: Function): Function {

        const methodExecutor = this;

        return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {

            descriptor.value = function () {

                if (this['isGatewayServiceWebSocket'] !== true) {

                    return Promise.reject(
                        new GatewayServiceError.Builder('Not a @GatewayServiceWebSocket class')
                        .withStatusCode(500)
                        .build()
                    );
                }

                const methodExecutionDescriptor: MethodExecutionDescriptor = {
                    target,
                    propertyKey,
                    arguments,
                };

                const message = WebSocketHelper.getWebSocketMessage(methodExecutionDescriptor) || null;

                const subscriptionCallback = WebSocketHelper.getSubscriptionCallback(methodExecutionDescriptor) || null;

                const webSocketMethodParameters: WebSocketMethodParameters = {
                    webSocketClient: this['webSocketClient'],
                    connectionHeaders: this['connectionHeaders'],
                    isDebugActive: this['isDebugActive'],
                    outgoingMessage: message,
                    subscriptionCallback: subscriptionCallback,
                };

                return methodResolver(webSocketMethodParameters);
            };
        };
    }
}
