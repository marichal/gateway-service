import { MethodExecutionDescriptor } from '../../types/method-execution-descriptor';
import { UrlHelper } from '../../helpers/url-helper';
import { MetadataHelper } from '../../helpers/metadata-helper';
import { HttpRequestHelper } from '../../helpers/http-request-helper';
import { HttpMethodParameters } from '../../types/http-method-parameters';
import { GatewayServiceError } from '../../errors/gateway-service-error';

export class HttpMethodExecutor  {

    private url: string;

    constructor(url: string) {

        this.url = url;
    }

    execute(methodResolver: Function): Function {

        const methodExecutor = this;

        return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {

            descriptor.value = function () {

                if (this['isGatewayService'] !== true) {

                    return Promise.reject(
                        new GatewayServiceError.Builder('Not a @GatewayService class')
                        .withStatusCode(500)
                        .build()
                    );
                }

                const methodExecutionDescriptor: MethodExecutionDescriptor = {
                    target,
                    propertyKey,
                    arguments,
                };

                let updatedUrl = (this['baseUrl'] || '') + methodExecutor.url;

                updatedUrl = UrlHelper.replacePathParameters(updatedUrl, methodExecutionDescriptor);

                updatedUrl = UrlHelper.addQueryParameters(updatedUrl, methodExecutionDescriptor);

                const mappingFunction: Function = MetadataHelper.getMappingMetadata(methodExecutionDescriptor) || null;

                const bodyObject = HttpRequestHelper.getBodyObject(methodExecutionDescriptor) || null;

                const httpMethodParameters: HttpMethodParameters = {
                    url: updatedUrl,
                    mapper: mappingFunction,
                    body: bodyObject,
                };

                return methodResolver(httpMethodParameters);
            };
        };
    }
}
